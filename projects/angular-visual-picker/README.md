<br/>

<p align="center">
    <img src="assets/VisualPicker.svg?sanitize=true" width="30%" alt="Visual Picker Logo">
</p>

<h1 align="center">
    Visual Picker
</h1>

<br/>

### 📦 Installation

```bash
$ yarn add angular-visual-picker
```

##### or

```bash
$ npm install angular-visual-picker --save
```

<br/>

### ⌨ Usage

here are a few quick examples to get you started, **it's all you need**!

1. Import the **AngularVisualPickerModule** into the current module.
   ```typescript
   import { AngularVisualPickerModule } from 'angular-visual-picker';
   ```
2. Register the module in the imports.

   ```typescript
   @NgModule({
     declarations: [AppComponent],
     imports: [BrowserModule, AngularVisualPickerModule],
     providers: [],
     bootstrap: [AppComponent]
   })
   export class AppModule {}
   ```

3. Use the **VisualPickerComponent** and the **VisualPickerOptionComponent** in the desired component.

#### single

Only a single option can be selected. This is the default behaviour.

```html
<div>
  <ng-visual-picker [(selected)]="singlePickerItem">
    <ng-visual-picker-option value="value1">test 1</ng-visual-picker-option>
    <ng-visual-picker-option value="value2">test 2</ng-visual-picker-option>
    <ng-visual-picker-option value="value3">test 3</ng-visual-picker-option>
  </ng-visual-picker>
  {{ singlePickerItem }}
</div>
```

#### multiple

If you want to use the picker as a multiple select you have to mark it as `multiple`.
In this case, `ng-visual-picker` will only work with arrays - accept arrays and propagate arrays.

```html
<div>
  <ng-visual-picker [(selected)]="multiPickerItems" multiple>
    <ng-visual-picker-option value="test1">test 1</ng-visual-picker-option>
    <ng-visual-picker-option value="test2">test 2</ng-visual-picker-option>
    <ng-visual-picker-option value="test3">test 3</ng-visual-picker-option>
  </ng-visual-picker>
  {{ multiPickerItems }}
</div>
```

### with label

#### disabled

```html
<ng-visual-picker disabled>
  <ng-visual-picker-option value="hello1">test 1</ng-visual-picker-option>
  <ng-visual-picker-option value="hello2">test 2</ng-visual-picker-option>
  <ng-visual-picker-option value="hello3">test 3</ng-visual-picker-option>
</ng-visual-picker>
```

##### or

```html
<ng-visual-picker>
  <ng-visual-picker-option value="hello1">test 1</ng-visual-picker-option>
  <ng-visual-picker-option value="hello2">test 2</ng-visual-picker-option>
  <ng-visual-picker-option value="hello3" disabled>test 3</ng-visual-picker-option>
</ng-visual-picker>
```

### Properties for `ng-visual-picker`

| Name           | Type         | Required | Default | Description                                                                                |
| :------------- | :----------- | :------- | :------ | :----------------------------------------------------------------------------------------- |
| `label`        | `string`     | Optional | `null`  | Sets the label of the picker group.                                                        |
| `multi`        | `boolean`    | Optional | `false` | If set the picker behaves as a group of checkboxes. If not set the picker behaves a radio. |
| `[(selected)]` | `T` or `T[]` | Optional | `null`  | Sets or returns the current value(s)                                                       |
| `disabled`     | `boolean`    | Optional | `false` | Activates the disabled state if true                                                       |

### Properties for `ng-visual-picker-option`

| Name       | Type      | Required | Default | Description                          |
| :--------- | :-------- | :------- | :------ | :----------------------------------- |
| `value`    | `string`  | Required | `null`  | Sets the value of the picker option. |
| `disabled` | `boolean` | Optional | `false` | Activates the disabled state if true |

### Licence

- License: MIT

### Authors

- **Tygo Driessen**
