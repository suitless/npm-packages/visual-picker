// tslint:disable:variable-name no-inferrable-types ban-types
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList
} from '@angular/core';
import { VisualPickerOptionComponent } from '../visual-picker-option/visual-picker-option.component';
import { ControlValueAccessor } from '@angular/forms';
import { filter, startWith, switchMap, takeWhile } from 'rxjs/operators';
import { merge } from 'rxjs';
import { VISUAL_PICKER_INJECTION_TOKEN } from '../visual-picker-injection-tokens';
import { convertToBoolProperty } from '../helpers';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ng-visual-picker',
  templateUrl: './visual-picker.component.html',
  styleUrls: ['./visual-picker.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: VISUAL_PICKER_INJECTION_TOKEN,
      useExisting: VisualPickerComponent
    }
  ]
})
export class VisualPickerComponent<T> implements AfterViewInit, AfterContentInit, OnDestroy, ControlValueAccessor {
  @Input() label: string;

  /**
   * Disables the picker
   */
  @Input()
  get disabled(): boolean {
    return !!this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = convertToBoolProperty(value);
  }
  protected _disabled: boolean;

  /**
   * Accepts selected item or array of selected items.
   */
  @Input()
  set selected(value: T | T[]) {
    this.writeValue(value);
  }
  get selected(): T | T[] {
    return this.multiple ? this.selectionModel.map(o => o.value) : this.selectionModel[0].value;
  }

  /**
   * Indicates whether or not multiple items can be selected
   */
  @Input()
  get multiple(): boolean {
    return this._multiple;
  }
  set multiple(value: boolean) {
    this._multiple = convertToBoolProperty(value);
  }
  protected _multiple: boolean = false;

  /**
   * Will be emitted when selected value changes.
   */
  @Output() selectedChange: EventEmitter<T | T[]> = new EventEmitter();

  /**
   * List of `VisualPickerOptionComponent`'s components passed as content.
   */
  @ContentChildren(VisualPickerOptionComponent, { descendants: true }) options: QueryList<
    VisualPickerOptionComponent<T>
  >;

  /**
   * List of selected options.
   */
  selectionModel: VisualPickerOptionComponent<T>[] = [];

  protected alive: boolean = true;

  /**
   * If a user assigns a value before content options rendered the value will be put in this variable.
   * And then applied after content rendered.
   * Only the last value will be applied.
   */
  protected queue: T | T[];

  /**
   * Function passed through control value accessor to propagate changes.
   */
  protected onChange: Function = () => {};
  protected onTouched: Function = () => {};

  constructor(protected cd: ChangeDetectorRef) {}

  ngAfterContentInit(): void {
    this.options.changes
      .pipe(
        takeWhile(() => this.alive),
        startWith(this.options),
        filter(() => this.queue != null && this.canSelectValue())
      )
      .subscribe(() => {
        // Call 'writeValue' when current change detection run is finished.
        // When writing is finished, change detection starts again, since
        // microtasks queue is empty.
        // Prevents ExpressionChangedAfterItHasBeenCheckedError.
        Promise.resolve().then(() => {
          this.writeValue(this.queue);
        });
      });
  }

  ngAfterViewInit(): void {
    this.subscribeOnOptionClick();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cd.markForCheck();
  }

  writeValue(value: T | T[]): void {
    if (!this.alive) {
      return;
    }

    if (this.canSelectValue()) {
      this.setSelection(value);
      if (this.selectionModel.length) {
        this.queue = null;
      }
    } else {
      this.queue = value;
    }
  }

  /**
   * Selects option or clear all selected options if value is null.
   */
  protected handleOptionClick(option: VisualPickerOptionComponent<T>) {
    this.queue = null;
    if (option.value == null) {
      this.reset();
    } else {
      this.selectOption(option);
    }

    this.cd.markForCheck();
  }

  /**
   * Deselect all selected options.
   */
  protected reset() {
    this.selectionModel.forEach((option: VisualPickerOptionComponent<T>) => option.deselect());
    this.selectionModel = [];
    this.emitSelected(this.multiple ? [] : null);
  }

  /**
   * Determines how to select a option; as multiple or single.
   */
  protected selectOption(option: VisualPickerOptionComponent<T>) {
    if (this.multiple) {
      this.handleMultipleSelect(option);
    } else {
      this.handleSingleSelect(option);
    }
  }

  /**
   * Handle single select option.
   */
  protected handleSingleSelect(option: VisualPickerOptionComponent<T>) {
    const selected = this.selectionModel.pop();

    if (selected && selected !== option) {
      selected.deselect();
    }

    this.selectionModel = [option];
    option.select();
    this.emitSelected(option.value);
  }

  /**
   * Handle multiple select option.
   */
  protected handleMultipleSelect(option: VisualPickerOptionComponent<T>) {
    if (option.selected) {
      this.selectionModel = this.selectionModel.filter(s => s.value !== option.value);
      option.deselect();
    } else {
      this.selectionModel.push(option);
      option.select();
    }

    this.emitSelected(this.selectionModel.map((opt: VisualPickerOptionComponent<T>) => opt.value));
  }

  protected subscribeOnOptionClick() {
    /**
     * If a user changes the provided options list during runtime
     * we have to handle and resubscribe on options.
     * Otherwise, the user will not be able to select new options.
     */
    this.options.changes
      .pipe(
        startWith(this.options),
        switchMap((options: QueryList<VisualPickerOptionComponent<T>>) => {
          return merge(...options.map(option => option.click));
        }),
        takeWhile(() => this.alive)
      )
      .subscribe((clickedOption: VisualPickerOptionComponent<T>) => this.handleOptionClick(clickedOption));
  }

  /**
   * Propagate selected event
   */
  protected emitSelected(selected: T | T[]) {
    this.onChange(selected);
    this.selectedChange.emit(selected);
  }

  /**
   * Set selected value in model
   */
  protected setSelection(value: T | T[]) {
    const isArray: boolean = Array.isArray(value);

    if (this.multiple && !isArray) {
      throw new Error(`Can't assign single value if picker is marked as multiple`);
    }

    if (!this.multiple && isArray) {
      throw new Error(`Can't assign array if picker is not marked as multiple`);
    }

    const previouslySelectedOptions = this.selectionModel;
    this.selectionModel = [];

    if (isArray) {
      (value as T[]).forEach((options: T) => this.selectValue(options));
    } else {
      this.selectValue(value as T);
    }

    // find options which were selected before and trigger deselect
    previouslySelectedOptions
      .filter((option: VisualPickerOptionComponent<T>) => !this.selectionModel.includes(option))
      .forEach((option: VisualPickerOptionComponent<T>) => option.deselect());

    this.cd.markForCheck();
  }

  /**
   * Selects value
   */
  protected selectValue(value: T) {
    const corresponding = this.options.find((option: VisualPickerOptionComponent<T>) => option.value === value);

    if (corresponding) {
      corresponding.select();
      this.selectionModel.push(corresponding);
    }
  }

  protected canSelectValue(): boolean {
    return !!(this.options && this.options.length);
  }
}
