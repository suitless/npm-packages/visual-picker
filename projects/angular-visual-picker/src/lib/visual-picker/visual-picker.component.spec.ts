import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualPickerComponent } from './visual-picker.component';

describe('VisualPickerComponent', () => {
  let component: VisualPickerComponent<any>;
  let fixture: ComponentFixture<VisualPickerComponent<any>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
