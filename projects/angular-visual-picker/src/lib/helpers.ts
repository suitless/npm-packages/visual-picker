let idCounter = 0;

export default function uniqueId(prefix: string) {
  idCounter += 1;
  return prefix ? `${prefix}-${idCounter}` : String(idCounter);
}

export function convertToBoolProperty(val: any): boolean {
  if (typeof val === 'string') {
    val = val.toLowerCase().trim();

    return (val === 'true' || val === '');
  }

  return !!val;
}
