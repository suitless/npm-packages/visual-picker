// tslint:disable:variable-name no-inferrable-types
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  Output,
  ViewChild
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { VisualPickerComponent } from '../visual-picker/visual-picker.component';
import { VISUAL_PICKER_INJECTION_TOKEN } from '../visual-picker-injection-tokens';
import { convertToBoolProperty } from '../helpers';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ng-visual-picker-option',
  templateUrl: './visual-picker-option.component.html',
  styleUrls: ['./visual-picker-option.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VisualPickerOptionComponent<T> implements OnDestroy {
  /**
   * Get a reference of the native input element
   */
  @ViewChild('inputElement') inputElement: ElementRef<HTMLInputElement>;

  /**
   * Option value that will be fired on selection.
   */
  @Input()
  value: T;

  @Input()
  get disabled(): boolean {
    return this._disabled || this.parent.disabled;
  }
  set disabled(value: boolean) {
    this._disabled = convertToBoolProperty(value);
  }
  protected _disabled: boolean = false;

  /**
   * Fires value on option selection change.
   */
  @Output() selectionChange: EventEmitter<VisualPickerOptionComponent<T>> = new EventEmitter();

  /**
   * Fires on option click.
   */
  protected click$: Subject<VisualPickerOptionComponent<T>> = new Subject<VisualPickerOptionComponent<T>>();
  get click(): Observable<VisualPickerOptionComponent<T>> {
    return this.click$.asObservable();
  }

  selected: boolean = false;
  protected parent: VisualPickerComponent<T>;
  protected alive: boolean = true;

  constructor(@Inject(VISUAL_PICKER_INJECTION_TOKEN) parent, protected cd: ChangeDetectorRef) {
    this.parent = parent;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  get multiple() {
    return this.parent.multiple;
  }

  @HostBinding('class.selected')
  get selectedClass(): boolean {
    return this.disabled;
  }

  @HostBinding('attr.disabled')
  get disabledAttribute(): '' | null {
    return this.disabled ? '' : null;
  }

  @HostBinding('tabIndex')
  get tabIndex() {
    return '-1';
  }

  @HostListener('click', [' $event'])
  @HostListener('keydown.space', ['$event'])
  @HostListener('keydown.enter', ['$event'])
  onClick(event: Event) {
    if (!this.disabled) {
      this.click$.next(this);

      // Prevent scroll on space click, etc.
      event.preventDefault();
    }
  }

  select() {
    this.setSelection(true);
  }

  deselect() {
    this.setSelection(false);
  }

  protected setSelection(selected: boolean) {
    if (this.alive && this.selected !== selected) {
      this.selected = selected;
      this.inputElement.nativeElement.checked = selected;
      this.selectionChange.emit(this);
      this.cd.markForCheck();
    }
  }
}
