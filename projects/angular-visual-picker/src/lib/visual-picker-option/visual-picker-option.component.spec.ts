import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualPickerOptionComponent } from './visual-picker-option.component';
import { VISUAL_PICKER_INJECTION_TOKEN } from '../visual-picker-injection-tokens';

function visualPickerOptionFactory() {
  return {
    HubConnectionBuilder: () => {
      return {
        withUrl: (url) => {
          return {
            build: () => { }
          };
        }
      };
    }
  };
}

const VISUAL_PICKER_PROVIDER = [ { provide: VISUAL_PICKER_INJECTION_TOKEN, useFactory: visualPickerOptionFactory } ];

describe('VisualPickerOptionComponent', () => {
  let component: VisualPickerOptionComponent<any>;
  let fixture: ComponentFixture<VisualPickerOptionComponent<any>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualPickerOptionComponent ],
      providers: [ VISUAL_PICKER_PROVIDER, VisualPickerOptionComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualPickerOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
