import { InjectionToken } from '@angular/core';

export const VISUAL_PICKER_INJECTION_TOKEN = new InjectionToken('VISUAL_PICKER_INJECTION_TOKEN');
