import { NgModule } from '@angular/core';
import { VisualPickerComponent } from './visual-picker/visual-picker.component';
import { VisualPickerOptionComponent } from './visual-picker-option/visual-picker-option.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [VisualPickerComponent, VisualPickerOptionComponent],
  imports: [
    CommonModule
  ],
  exports: [VisualPickerComponent, VisualPickerOptionComponent]
})
export class AngularVisualPickerModule {}
