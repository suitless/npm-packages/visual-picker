import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularVisualPickerModule } from '../../../angular-visual-picker/src/lib/angular-visual-picker.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularVisualPickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
